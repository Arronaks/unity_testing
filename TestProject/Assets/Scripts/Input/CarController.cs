using UnityEngine.InputSystem;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CarController : MonoBehaviour
{
    public Rigidbody sphereRb;  // Rigidbody of the sphere, that will move our car
    public List<GameObject> wheels = new List<GameObject> ();
    public List<bool> groundedBools = new List<bool> ();
    public Rigidbody carRigidbody;

    private Inputs inputs;      // Inputs of the player
    private float moveInput;    // This will control vertical axis input
    private float turnInput;    // This will control horizontal axis input
    private PlayerInput playerInputs;  //This will look for device that player is using

    public float forwardSpeed;  // Speed of the car moving forvard
    public float reverseSpeed;  // Reverse speed of the car
    public float turnSpeed;     // Speed of turning
    public float turnSpeedInAir;
    public float lowestSpeedOnTurn; //Lowest speed when car constantly turning
    public float timeToReachLSOT;   // Time to reach lowest speed on turn
    private float _timeToReachLSOT;
    private float deltaTimeLSOT = 0.2f;
    private float accelerationTime = 1f;
    private float _accelerationTime;

    public float airDrag;       // Drag of sphere in the air
    public float groundDrag;    // Drag of the sphere on the ground
    public float gravity = -9.18f;
    public float allignToGroundTime = 20f;

    public float jumpTimeout = 2f;
    private float _jumpTimeout;
    public float jumpHeight = 10f;
    private float _verticalVelocity;

    public float sprintMulti =2f;
    public float boostTime = 2f;
    private float _boostTime;
    private bool isSprintNotAvailiable;

    private RaycastHit groundRayHit;
    private RaycastHit downRayHit;
    public float maxRayLength;
    private float rayForGrounding = 2f;
    private float _maxRayLength;
    private float halfOfRay;
    public bool isGrounded;
    public LayerMask groundLayer;
    private float timeForGrounding = 10f;
    private float _turnSpeed;
    public float timeToRotate = 10f;

    public List<TrailRenderer> trailRenderers = new List<TrailRenderer>();

    private void Start()
    {
        sphereRb.transform.parent = null;
        carRigidbody.transform.parent = null;
        inputs = GetComponent<Inputs>();
        _maxRayLength = maxRayLength;
        halfOfRay = maxRayLength / 2f;
        _timeToReachLSOT = timeToReachLSOT;
        _accelerationTime = accelerationTime;
        _turnSpeed = turnSpeed;
        _jumpTimeout = jumpTimeout;
        _boostTime = boostTime;
            }

    private void Update()
    {
        MoveCar();
    }

    private void FixedUpdate()
    {
        MoveSphere();
        HandleJump();
    }


    private void MoveCar()
    {
        moveInput = inputs.move.y;
        turnInput = inputs.move.x;

        //adjusting speed of car
        moveInput *= moveInput > 0 ? forwardSpeed : reverseSpeed;
        if (inputs.move.y != 0)
        {
            Quaternion zRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, zRotation, Time.deltaTime * timeToRotate);
            if (inputs.move.x != 0)
            {

                //moveInput = Mathf.MoveTowards(moveInput, lowestSpeedOnTurn, lowestSpeedOnTurn);
                float _moveInput = Mathf.Lerp(lowestSpeedOnTurn, moveInput, _timeToReachLSOT);
                moveInput = _moveInput;
                _timeToReachLSOT -= deltaTimeLSOT * Time.deltaTime;
                //Debug.Log(_timeToReachLSOT);
            }
            else
            {
                _timeToReachLSOT = timeToReachLSOT;
            }
        }
        

        // set cars position to sphere
        transform.position = sphereRb.transform.position;
        // raycast ground check
        for (int i = 0; i < wheels.Count; i++)
        {
            
            groundedBools[i] = Physics.Raycast(wheels[i].transform.position, -transform.up, out groundRayHit, maxRayLength, groundLayer);
        }
        if (groundedBools[0] == true || groundedBools[1] == true || groundedBools[2] == true || groundedBools[3] == true)
        {
            isGrounded = true;
            turnSpeed = _turnSpeed;
            //foreach (TrailRenderer trail in trailRenderer)
            //{
            //    trail.emitting = true;
            //}
        }
        else
        {
            isGrounded = false;
            
            turnSpeed = turnSpeedInAir;
            //foreach (TrailRenderer trail in trailRenderer)
            //{
            //    trail.emitting = false;
            //}
        }
        for (int i = 0; i < wheels.Count; i++)
        {
            if (groundedBools[i] == true && inputs.move.y != 0)
                trailRenderers[i].emitting = true;
            else
                trailRenderers[i].emitting = false;
        }
        //isGrounded = Physics.Raycast(transform.position, -transform.up, out groundRayHit, maxRayLength, groundLayer);


        //if (isGrounded)
        //{
            Physics.Raycast(sphereRb.transform.position, -transform.up, out downRayHit, rayForGrounding, groundLayer);
            // set cars rotation
            float newRotation = turnInput * turnSpeed * Time.deltaTime * inputs.move.y;
            transform.Rotate(0, newRotation, 0, Space.World);
            //rotating car to be parallel to ground
            Quaternion toRotateTo = Quaternion.FromToRotation(transform.up, downRayHit.normal) * transform.rotation;
            //float angle = Vector3.Angle(-transform.up, downRayHit.normal);
            transform.rotation = Quaternion.Slerp(transform.rotation, toRotateTo, allignToGroundTime * Time.deltaTime);
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotateTo, angle * Time.deltaTime * allignToGroundTime);
        //}
        //Drag on ground or in air
        switch (isGrounded)
        {
            case true:
                sphereRb.drag = groundDrag;
                break;
            case false:
                sphereRb.drag = airDrag;
                break;
        }
        Debug.DrawRay(sphereRb.transform.position, -transform.up, Color.blue, maxRayLength);
        for (int i = 0; i < wheels.Count; i++)
        {
            Debug.DrawRay(wheels[i].transform.position, -transform.up, Color.red, maxRayLength); ;
        }
        


    }

    private void MoveSphere()
    {
        if (isGrounded)
        {
            if (inputs.sprint == true && boostTime >= 0f)
            {
                sphereRb.AddForce(transform.forward * moveInput * sprintMulti, ForceMode.Acceleration);
                boostTime -= Time.fixedDeltaTime;
            }
            if (boostTime <= 0f || inputs.sprint == false)
            {
                inputs.sprint = false;
                boostTime = _boostTime;
                sphereRb.AddForce(transform.forward * moveInput, ForceMode.Acceleration);
            }
        }
        else
        {

            sphereRb.AddForce(transform.up * gravity);
            maxRayLength = halfOfRay;
        }

        
        Debug.Log(boostTime);
        carRigidbody.MoveRotation(transform.rotation);
        //carRigidbody.transform.position = transform.position;

        //for (int i = 0; i < wheels.Count; i++)
        //{
        //    if (groundedBools[i] == false)
        //    {
        //        sphereRb.AddForceAtPosition(-transform.up * gravity / wheels.Count, wheels[i].transform.position);
        //    }
        //}
    }


    private void HandleJump()
    {
        if (inputs.jump && _jumpTimeout >= 0f)
        {
            VerticalJump();
            _jumpTimeout -= Time.deltaTime;
        }
        if (_jumpTimeout <= 0f && isGrounded)
        {
            inputs.jump = false;
            _jumpTimeout = jumpTimeout;
            sphereRb.AddForce(0f, -_verticalVelocity, 0f);
            _verticalVelocity = 0f;
        }
        //Debug.Log(_jumpTimeout);
    }

    public void VerticalJump()
    {
        _verticalVelocity = Mathf.Sqrt(jumpHeight * -2f * gravity);
        sphereRb.AddForce(0f, _verticalVelocity, 0f);
    }
    }
