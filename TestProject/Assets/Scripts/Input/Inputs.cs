using UnityEngine;
using UnityEngine.InputSystem;

public class Inputs : MonoBehaviour
{
    //Character input values
    public Vector2 move;
    public Vector2 look;
    public bool jump;
    public bool sprint;
    public bool attack;
    public bool escape;

    //Movement settings
    public bool analogMovement;

    //Mouse cursor settings
    public bool cursorLocked = true;
    public bool cursorInputForLook = true;


//#if ENABLE_INPUT_SYSTEM
    public void OnMove(InputValue value)
    {
        MoveInput(value.Get<Vector2>());
    }

    public void OnLook(InputValue value)
    {
        if (cursorInputForLook)
        {
            LookInput(value.Get<Vector2>());
        }
        //Debug.Log("Looking");
    }

    public void OnJump(InputValue value)
    {
        JumpInput(value.isPressed);
    }

    public void OnSprint(InputValue value)
    {
        SprintInput(value.isPressed);
    }
    public void OnAttack(InputValue value)
    {
        AttackInput(value.isPressed);
    }
    public void OnEscape(InputValue value)
    {
        EscapeInput(value.isPressed);
    }

//#endif


    public void MoveInput(Vector2 newMoveDirection)
    {
        move = newMoveDirection;
    }
    public void LookInput(Vector2 newLookDirection)
    {
        look = newLookDirection;
    }
    public void JumpInput(bool newJumpState)
    {
        jump = newJumpState;
    }
    public void SprintInput(bool newSprintState)
    {
        sprint = newSprintState;
    }

    public void AttackInput(bool newAttakState)
    {
        attack = newAttakState;
    }
    private void OnApplicationFocus(bool hasFocus)
    {
        SetCursorState(cursorLocked);
    }
    public void EscapeInput(bool newEscapeState)
    {
        escape = newEscapeState;
    }

    private void SetCursorState(bool newState)
    {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }
}
