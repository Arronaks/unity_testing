using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class V2CarControl : MonoBehaviour
{
    [Header("Motor/Wheels/Car Rigidbody/Trails")]
    public  Rigidbody sphereRb;  // Rigidbody of the sphere, that will move our car
    public  List<GameObject> wheels = new List<GameObject>();
    public  Rigidbody carRigidbody;
    public List<TrailRenderer> trailRenderers = new List<TrailRenderer>();
    public List<bool> groundedBools = new(4);
    public List<ParticleSystem> particleBoost = new List<ParticleSystem>();

    #region Inputs
    private Inputs inputs;      // Inputs of the player
    private float moveInput;    // This will control vertical axis input
    private float turnInput;    // This will control horizontal axis input
    private PlayerInput playerInputs;  //This will look for device that player is using
    #endregion

    #region Speed Settings
    [Header("Speed Settings")]
    public float  forwardSpeed = 200f;             // Speed of the car moving forvard
    public float  reverseSpeed = 100f;             // Reverse speed of the car
    public float  turnSpeed = 158f;                // Speed of turning
    public float  turnSpeedInAir = 80f;
    public float  sprintMultiplier = 2f;     // Speed multiplier on boost
    public float  boostTime = 2f;            // Length of the boost
    public float boostCooldown =10f;
    private float _boostCooldown;
    private float _boostTime;
    private bool isSprinting;               // This will check if car in boost
    private float lowestSpeedOnTurn = 100f; // Lowest speed when car constantly turning
    private float timeToReachLSOT = 1f;     // Time to reach lowest speed on turn
    private float _timeToReachLSOT;
    private float deltaTimeLSOT = 0.2f;
    private float accelerationTime = 1f;
    private float _accelerationTime;
    #endregion

    #region Jump and Air Time
    [Header ("Jump and Air Time")]
    public float jumpTimeout = 0.3f;
    public float jumpHeight = 200f;
    public float airDrag = 0.1f;                   // Drag of sphere in the air
    public float groundDrag = 5f;                // Drag of the sphere on the ground
    public float gravity = -9.18f;
    public float allignToGroundTime = 4f;
    public float timeToRotate = 10f;
    private float _jumpTimeout;
    private float _verticalVelocity;
    #endregion

    #region Raycast and GroundCheck
    [Header("Ground Check")]
    public LayerMask groundLayer;
    public bool isGrounded;
    public float maxRayLength = 1f;
    private RaycastHit groundRayHit;
    private RaycastHit downRayHit;
    private float rayForGrounding = 2f;
    private float halfOfRay;
    private float _turnSpeed;
    #endregion

    private void Start()
    {
        DetachRigidbodyFromParents(sphereRb);
        DetachRigidbodyFromParents(carRigidbody);
        inputs = GetComponent<Inputs>();
        halfOfRay = maxRayLength / 2f;
        SetLinksWithFloatObjects();
    }
    private void Update()
    {
        MoveCar();
        //EnableDisableParticleSystem();
        EnableTrail();
    }
    private void FixedUpdate()
    {
        MoveSphere();
        HandleJump();
    }
    private void MoveCar()
    {
        moveInput = inputs.move.y;
        turnInput = inputs.move.x;

        //adjusting speed of car
        moveInput *= moveInput > 0 ? forwardSpeed : reverseSpeed;
        if (inputs.move.y != 0)
        {
            //if inputs are in, rotate car to 0 on Z. Fix for getting stuck
            if (CheckForFalse(groundedBools) == false)
            {
                StartCoroutine(RotateTowardsZ());
            }
            if (inputs.move.x != 0 && isSprinting == false)
            {
                float _moveInput = Mathf.Lerp(lowestSpeedOnTurn, moveInput, _timeToReachLSOT);
                moveInput = _moveInput;
                _timeToReachLSOT -= deltaTimeLSOT * Time.deltaTime;
            }
            else
            {
                _timeToReachLSOT = timeToReachLSOT;
            }
        }


        // set cars position to sphere
        transform.position = sphereRb.transform.position;
        // raycast ground check
        for (int i = 0; i < wheels.Count; i++)
        {

            groundedBools[i] = Physics.Raycast(wheels[i].transform.position, -transform.up, out groundRayHit, maxRayLength, groundLayer);
        }
        if (CheckForTrue(groundedBools))
            /*(groundedBools[0] == true || groundedBools[1] == true || groundedBools[2] == true || groundedBools[3] == true)*/
        {
            isGrounded = true;
            turnSpeed = _turnSpeed;
        }
        else
        {
            isGrounded = false;
            turnSpeed = turnSpeedInAir;
        }
        Physics.Raycast(sphereRb.transform.position, -transform.up, out downRayHit, rayForGrounding, groundLayer);
        // set cars rotation
        float newRotation = turnInput * turnSpeed * Time.deltaTime * inputs.move.y;
        transform.Rotate(0, newRotation, 0, Space.World);
        Quaternion toRotateTo = Quaternion.FromToRotation(transform.up, downRayHit.normal) * transform.rotation;
        transform.rotation = Quaternion.Slerp(transform.rotation, toRotateTo, allignToGroundTime * Time.deltaTime);
        // Handle drag depends on isGrounded
        switch (isGrounded)
        {
            case true:
                sphereRb.drag = groundDrag;
                break;
            case false:
                sphereRb.drag = airDrag;
                break;
        }
    }

    private void HandleJump()
    {
        if (inputs.jump && _jumpTimeout >= 0f)
        {
            VerticalJump();
            _jumpTimeout -= Time.deltaTime;
            StartCoroutine(RotateTowardsZ());
        }
        if (_jumpTimeout <= 0f && isGrounded)
        {
            inputs.jump = false;
            _jumpTimeout = jumpTimeout;
            sphereRb.AddForce(0f, -_verticalVelocity, 0f);
            _verticalVelocity = 0f;
        }
    }

    public void VerticalJump()
    {
        _verticalVelocity = Mathf.Sqrt(jumpHeight * -2f * gravity);
        sphereRb.AddForce(0f, _verticalVelocity, 0f);
    }

    private void MoveSphere()
    {
        if (isGrounded)
        {
            if (boostCooldown >= 0f && inputs.sprint == true)
            {
                if (boostTime >= 0f)
                {
                    sphereRb.AddForce(transform.forward * moveInput * sprintMultiplier, ForceMode.Acceleration);
                    boostTime -= Time.fixedDeltaTime;
                    isSprinting = true;
                }
                if (boostTime <= 0f)
                {
                    sphereRb.AddForce(transform.forward * moveInput, ForceMode.Acceleration);
                    isSprinting = false;
                }
                boostCooldown -= Time.fixedDeltaTime;
            }
            if (boostCooldown <= 0f || inputs.sprint == false)
            {
                inputs.sprint = false;
                boostTime = _boostTime;
                boostCooldown = _boostCooldown;
                sphereRb.AddForce(transform.forward * moveInput, ForceMode.Acceleration);
            }
        }
        else
        {
            sphereRb.AddForce(transform.up * gravity);
            maxRayLength = halfOfRay;
        }
        Debug.Log(boostTime);
        carRigidbody.transform.position = transform.position;
        carRigidbody.MoveRotation(transform.rotation);
    }

    private void DetachRigidbodyFromParents(Rigidbody rb)
    {
        rb.transform.parent = null;
       
        
    }
    private void SetLinksWithFloatObjects()
    {
        _timeToReachLSOT = timeToReachLSOT;
        _accelerationTime = accelerationTime;
        _turnSpeed = turnSpeed;
        _jumpTimeout = jumpTimeout;
        _boostTime = boostTime;
        _boostCooldown = boostCooldown;
    }


    private void EnableDisableParticleSystem()
    {
        switch (inputs.sprint)
        {
            case true:
                for (int i = 0; i < particleBoost.Count; i++)
                {
                    particleBoost[i].Play();
                }
                break;
            case false:
                for (int i = 0; i < particleBoost.Count; i++)
                {
                    particleBoost[i].Stop();
                }
                break;
        }
    }

    IEnumerator RotateTowardsZ()
    {
            yield return new WaitForSeconds(2f);
            Quaternion zRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, zRotation, Time.deltaTime * timeToRotate);
    }

    private bool CheckForFalse(List<bool> list)
    {
        bool _check = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == false)
            {
                _check = false;
                break;
            }
            else
            {
                _check = true;
            }
        }
        return _check;
    }       //This and next method are gonna check if any ground ray is false/true and return result
    private bool CheckForTrue(List<bool> list)
    {
        bool _check = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == true)
            {
                _check = true;
                break;
            }
            else
            {
                _check = false;
            }
        }
        return _check;
    }


    private void EnableTrail()
    {
        for (int i =0; i < wheels.Count; i++)
        {
            if (groundedBools[i] == true)
                trailRenderers[i].emitting = true;
            else
                trailRenderers[i].emitting = false;
        }
    }
}
